const express = require("express");
const JobListingsService = require("./Services/JobListingsService");

const app = express();
app.use( (req, res, next) =>
{
    res.setHeader('Access-Control-Allow-Origin', 'http://localhost:3000');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
    res.setHeader('Access-Control-Allow-Credentials', true);

    next();
});

const listingsService = new JobListingsService();
app.get("/listings", listingsService.getListings);

const server = app.listen(8081);