/**
 * Hard-coded Listing objects.
 * @type {Array<Listing>}
 */
const LISTINGS = [
    {
        id: 0,
        title: "Account Manager",
        date: 1568759627451,
        category: "Management",
        description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt " +
                     "ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation " +
                     "ullamco laboris nisi ut aliquip ex ea commodo consequat",
        nationality: "UAE",
        salary: "AED 8000",
        lastDate: 1568759627451
    },
    {
        id: 1,
        title: "Full-Stack Developer",
        date: 1568759627451,
        category: "Development",
        description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt " +
                     "ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation " +
                     "ullamco laboris nisi ut aliquip ex ea commodo consequat",
        nationality: "UAE",
        salary: "AED 8500",
        lastDate: 1568759627451
    },
    {
        id: 2,
        title: "Account Manager",
        date: 1568759627451,
        category: "Management",
        description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt " +
                     "ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation " +
                     "ullamco laboris nisi ut aliquip ex ea commodo consequat",
        nationality: "UAE",
        salary: "AED 8000",
        lastDate: 1568759627451
    },
    {
        id: 3,
        title: "Account Manager",
        date: 1568759627451,
        category: "Management",
        description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt " +
                     "ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation " +
                     "ullamco laboris nisi ut aliquip ex ea commodo consequat",
        nationality: "UAE",
        salary: "AED 8000",
        lastDate: 1568759627451
    },
    {
        id: 4,
        title: "Full-Stack Developer",
        date: 1568759627451,
        category: "Development",
        description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt " +
                     "ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation " +
                     "ullamco laboris nisi ut aliquip ex ea commodo consequat",
        nationality: "UAE",
        salary: "AED 8500",
        lastDate: 1568759627451
    },
    {
        id: 5,
        title: "Account Manager",
        date: 1568759627451,
        category: "Management",
        description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt " +
                     "ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation " +
                     "ullamco laboris nisi ut aliquip ex ea commodo consequat",
        nationality: "UAE",
        salary: "AED 8000",
        lastDate: 1568759627451
    },
    {
        id: 6,
        title: "Account Manager",
        date: 1568759627451,
        category: "Management",
        description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt " +
                     "ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation " +
                     "ullamco laboris nisi ut aliquip ex ea commodo consequat",
        nationality: "UAE",
        salary: "AED 8000",
        lastDate: 1568759627451
    },
    {
        id: 7,
        title: "Full-Stack Developer",
        date: 1568759627451,
        category: "Development",
        description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt " +
                     "ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation " +
                     "ullamco laboris nisi ut aliquip ex ea commodo consequat",
        nationality: "UAE",
        salary: "AED 8500",
        lastDate: 1568759627451
    },
    {
        id: 8,
        title: "Account Manager",
        date: 1568759627451,
        category: "Management",
        description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt " +
                     "ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation " +
                     "ullamco laboris nisi ut aliquip ex ea commodo consequat",
        nationality: "UAE",
        salary: "AED 8000",
        lastDate: 1568759627451
    }
];

class ListingDAO
{

    /**
     * Retrieve listing objects from the store.
     *
     * @return {Array<Listing>}
     */
    retrieveListings( /* Object */ filters)
    {
        filters = filters || {};
        return LISTINGS.filter( listing =>
        {
            return Object.keys(filters).reduce((result, current) =>
            {
               return result && listing[current] === filters[current];
            }, true);
        });
    }

}

module.exports = ListingDAO;