const ListingDAO = require("../DataAccessObjects/ListingDAO");
const listingDAO = new ListingDAO();

class JobListingsService
{

    getListings(/* Request */ req, /* Response */ res) {
        res.json(listingDAO.retrieveListings(req.query));
    }

}

module.exports = JobListingsService;