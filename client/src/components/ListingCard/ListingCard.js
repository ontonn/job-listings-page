import React from 'react';
import './ListingCard.css';

class ListingCard extends React.Component
{
    state = {
        listing: null
    };

    constructor(props) {
        super(props);
        this.state.listing = props.listing;
    }

    render()
    {
        return (
        <div className="listingContainer">
            <div className="listingIcon"><img src="icons/wreath.svg"/></div>
            <div className="listingContent">
                <div>
                    <div className="listingTitleContainer">
                        <div className="listingTitle">{this.state.listing.title.toUpperCase()}</div>
                        <div className="listingDate">{this.formatDate( this.state.listing.date )}</div>
                    </div>
                    <div className="listingCategory">{this.state.listing.category.toUpperCase()}</div>
                </div>
                <div className="listingDescription">{this.state.listing.description}</div>
                <div className="listingLabels">
                    <div className="labelColumn">
                        <div className="labelHeader">NATIONALITY</div>
                        <div>{this.state.listing.nationality}</div>
                    </div>
                    <div className="labelColumn">
                        <div className="labelHeader">SALARY</div>
                        <div>{this.state.listing.salary}</div>
                    </div>
                    <div className="labelColumn">
                        <div className="labelHeader">LAST DATE</div>
                        <div>{this.formatDate( this.state.listing.lastDate )}</div>
                    </div>
                </div>
            </div>
            <div className="buttonContainer">
                <button>APPLY AND OFFER</button>
            </div>
        </div>
        );
    }


    formatDate(timestamp) {
        const date = new Date(timestamp);
        return date.getDay() + "/" + date.getMonth() + "/" + date.getFullYear();
    }
}

export default ListingCard;