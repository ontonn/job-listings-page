import React from 'react';
import ListingCard from '../ListingCard/ListingCard.js'
import axios from 'axios';
import './ListingsCollection.css';

class ListingsCollection extends React.Component
{
    state = {
        listings: [],
        category: ""
    };

    render()
    {
        return (
            <div>
                <form className="categoryForm">
                    <label>
                        <input type="radio" value="" checked={this.state.category === ""}
                               onChange={this.handleCategoryChange}/>
                        All jobs
                    </label>
                    <label>
                        <input type="radio" value="Management" checked={this.state.category === "Management"}
                               onChange={this.handleCategoryChange}/>
                        Manegerial
                    </label>
                    <label>
                        <input type="radio" value="Development" checked={this.state.category === "Development"}
                               onChange={this.handleCategoryChange}/>
                        Development
                    </label>
                </form>
                {this.state.listings.map( listing => <ListingCard listing={listing} key={listing.id}/>)}
            </div>
        );
    }

    componentDidMount() {
        this.getListings();
    }

    getListings = () =>
    {
        let url = "http://localhost:8081/listings";
        if(this.state.category.length > 0)
        {
            url += "?category=" + this.state.category;
        }
        axios.get(url)
             .then(res =>
             {
                 this.setState({listings: res.data});
             });
    };

    handleCategoryChange = event =>
    {
        this.setState( {
            category : event.target.value
        }, () => this.getListings() );
    };

}

export default ListingsCollection;
