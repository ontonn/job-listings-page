import React from 'react';
import ReactDOM from 'react-dom';
import ListingsCollection from './components/ListingsCollection/ListingsCollection';
import * as serviceWorker from './serviceWorker';
import './index.css';

function App() {
    return (
        <div className="container">
            <figure className="bannerFigure"><img src="images/banner.jpg"/></figure>
            <ListingsCollection />
        </div>
    );
}
ReactDOM.render(<App />, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
