# Starting up the server
1. cd server && npm install
2. node server.js

Server port is 8081.

# Starting up the client
1. cd client && npm install
2. npm start

App port is 3000.